### **ARCH IDE and the TNX Instruction Set** ###

**Overview**

This project demonstrates the use of a customized instruction set. The Arch IDE is a python (tkinter) based application that can process instructions as described below. The application can be run as is once the required libraries have been installed.

- The Arch IDE hosts the TNX instruction set. The TNX instruction set is a 64 - bit instruction set. This
    allows larger memory spaces to be accessed, and larger data values to be stored and retrieved with
    each instruction.
- All memory is aligned, i.e., data is stored in blocks of eight bytes, following the little-endian byte
    order.
- The instruction opcode is 7 bits long, meaning that there are 128 possible instructions.
- Some instruction like sizeof, push, and pop, etc. that are a combination of multiple MIPS
    instructions.
- There are 32 registers, meaning that the operands take up the at most the next 10 bits (5 + 5 bits),
    leaving the rest to data, and other values needed for respective instructions. The breakdown of
    registers is as follows:
 -- “zer” is a dedicated register holding the value zero
 -- “gv1” through “gv1 4 ” are general purpose (general value) registers, which include temporary values saved, results of functions called, and arguments stored
 --  “sd1” through “sd8” are registers that can be used to store values that are preserved across a procedure call
- reserved registers include:
          ▪ “asr” is the assembler register
          ▪ “kr1” is the OS kernel register #
          ▪ “kr2” is the OS kernel register #
          ▪ “fpp” is the frame pointer
          ▪ “gpr” points to values that are stored in an area that can be accessed globally
          ▪ “stp” is the stack pointer
          ▪ “rta” is the register that is used to storing the return address
          ▪ “out” set register with value to print
          ▪ “rem” holds the remainder from div instructions
- Two’s complement processing helps convert signed numbers in instructions.

Some examples are shown below:

* Add-Multiply

![Add-Multiply](images/example1.PNG)

* Array

![Array](images/example2.PNG)

* Branch

![Branch](images/example3.PNG)


**The instructions are listed below and are organized into the following formats:**

1. Data Operations

* the suffix “u“ stands for unsigned
* imm stands for immediate value
* fp here stands for floating-point

```
add_imm gv1, gv2, imm
add_immu gv1, gv2, immu
add_reg gv1, gv2, gv
add_regu gv1, gv2, gv
```
```
sub_reg gv1, gv2, gv
sub_regu gv1, gv2, gv
```
```
mul_reg gv1, gv2, gv
mul_regu gv1, gv2, gv
```
```
div_reg gv1, gv2, gv
div_regu gv1, gv2, gv
```
```
fpadd_imm gv1, gv2, imm
fpadd_immu gv1, gv2, immu
fpadd_reg gv1, gv2, gv
fpsub_reg gv1, gv2, gv
fpmul_reg gv1, gv2, gv
fpdiv_reg gv1, gv2, gv
fpadd_regu gv1, gv2, gv
fpsub_regu gv1, gv2, gv
fpmul_regu gv1, gv2, gv
fpdiv_regu gv1, gv2, gv
```

2. Data Transfer
```
ld_wrd gv3, imm(gv2)
   where the immediate is the offset for the
   base address to load from
st_wrd gv3, imm(gv2)
   where the immediate is the offset for the
   base address to store to
ld_add gv1, array or string
ld_byte gv1, gv
cpy gv1, gv
   copies content from gv2 to gv
str_add gv1, gv2, gv
   concat two strings
str_cmp gv1, gv2, gv
   compare two string
pop gv
   retrieve from stack
push gv
   push onto stack
```

3. Branching and Sequencing
For branch operations, the following suffixes are as follows:
- e – equal to
- g – greater than
- l – less than
- ge – greater than or equal to
- le – less than or equal to
- n – stands for not
```
brnch string_location
brnch_ge gv1, gv2, string_location
brnch_le gv1, gv2, string_location
brnch_g gv1, gv2, string_location
Brnch_l gv1, gv2, string_location
brnch_nge gv1, gv2, string_location
brnch_nle gv1, gv2, string_location
brnch_ng gv1, gv2, string_location
brnch_nl gv1, gv2, string_location
brnch_z gv1, gv2, string_location
brnch_nz gv1, gv2, string_location
```
```
jmp string_location
jmp_a gv1, imm, string_location
jump if above immediate value
jmp_b gv1, imm, string_location
jump if below immediate value
Jmp_e jump if equal to immediate value
```

Other instructions:

```
inc gv
increase by 1
dec gv
decrease by 1
fpinc gv
increase floating point value by 1.
fpdec gv
decrease floating point value by 1.
sizeof_arr len of array
sizeof_str len of string
no_op no operation
print_call print result to out – system call
```