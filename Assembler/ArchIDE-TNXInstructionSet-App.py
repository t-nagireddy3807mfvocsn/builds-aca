
# import all gui modules
import copy
import re
from tkinter import *
import pandas as pd
import binascii


class AssemblyGUI:

    def __init__(self, master=None):

        # gui objects
        self.master = master
        self.create_widgets()

        # assembly objects
        self._result = None

        # set limitations (number of rows (hexadecimal addresses) and columns (each 4 bytes))
        mem_address_indexes = [hex(x) for x in range(268500992, 268501080, 8)]
        stack_address_indexes = [hex(x) for x in range(268501080, 268501144, 8)]
        columns_ = [hex(4)]

        # create a dataframe with a bunch of memory addresses
        self.memory_addresses = pd.DataFrame(index=mem_address_indexes, columns=columns_).fillna(hex(0))
        self.memory_addresses_empty = copy.deepcopy(self.memory_addresses)

        # stack operations
        self.stack = pd.DataFrame(index=stack_address_indexes, columns=columns_).fillna(hex(0))
        self.stack_empty = copy.deepcopy(self.stack)
        self.stack_items = []

        self.last_address = 0  # cell of next contiguous address
        self.last_address_empty = copy.deepcopy(self.last_address)

        # arrays and registers
        self.memory_arrays = {}
        self.memory_arrays_empty = copy.deepcopy(self.memory_arrays)

        self.memory_references = {}
        self.memory_references_empty = copy.deepcopy(self.memory_references)

        self.registers = {
            "sd1": 0,
            "sd2": 0,
            "sd3": 0,
            "sd4": 0,
            "sd5": 0,
            "sd6": 0,
            "sd7": 0,
            "sd8": 0,
            "zer": 0,
            "asr": '',
            "kr1": '',
            "kr2": '',
            "fpp": '',
            "gpr": 0,
            "stp": 0,
            "rta": 0,
            "gv1": 0,
            "gv2": 0,
            "gv3": 0,
            "gv4": 0,
            "gv5": 0,
            "gv6": 0,
            "gv7": 0,
            "gv8": 0,
            "gv9": 0,
            "gv10": 0,
            "gv11": 0,
            "gv12": 0,
            "gv13": 0,
            "gv14": 0,
            "gv15": 0,
            "fp5": 0,
            "fp6": 0,
            "out": 0,
            "rem": 0,
            "ctrl": 0,
            "ovfl": 0,
            "crry": 0
        }
        self.registers_empty = copy.deepcopy(self.registers)

        regs_wout_ctrl = copy.deepcopy(self.registers)
        del regs_wout_ctrl['ctrl']

        self.textbox_results.insert(END, 'line 0: {}\nOUT:: {}\n\n'.format(regs_wout_ctrl, self._result))
        self.textbox_info.insert(END, 'line 0:\n{}\n\n{}\n\n'.format(self.memory_addresses.to_string(), self.stack.to_string()))

    def create_widgets(self):

        # tool bar
        self.button_run = Button(self.master, text="Run", width=8, height=1, command=self.run_program)
        self.button_clear = Button(self.master, text="Clear Results", width=8, height=1, command=self.clear_results)
        self.button_run.grid(column=0, row=0, sticky='nsew')
        self.button_clear.grid(column=34, row=0, sticky='nsew', columnspan=6)

        # text box to write instructions/code and text box to view results, and textbox to show info
        self.textbox_edit = Text(self.master, height=20, width=100, wrap='none')
        self.textbox_info = Text(self.master, height=20, width=40, wrap='none')
        self.textbox_results = Text(self.master, height=10, width=100, wrap='none')
        self.textbox_edit.grid(column=0, row=1, columnspan=24)
        self.textbox_info.grid(column=26, row=1, columnspan=12)
        self.textbox_results.grid(column=0, row=3, columnspan=38, sticky='nsew')

        # scroll bar in editing text box
        vertscroll_edit = Scrollbar(self.master, orient='vertical')
        horizscroll_edit = Scrollbar(self.master, orient="horizontal")
        vertscroll_edit.config(command=self.textbox_edit.yview)
        horizscroll_edit.config(command=self.textbox_edit.xview)
        self.textbox_edit.config(yscrollcommand=vertscroll_edit.set)
        self.textbox_edit.config(xscrollcommand=horizscroll_edit.set)
        vertscroll_edit.grid(column=25, row=1, sticky='ns')
        horizscroll_edit.grid(column=0, row=2, sticky='nsew', columnspan=24)

        # scroll bar in info text box
        vertscroll_info = Scrollbar(self.master)
        vertscroll_info.config(command=self.textbox_info.yview)
        self.textbox_edit.config(yscrollcommand=vertscroll_info.set)
        vertscroll_info.grid(column=39, row=1, sticky='ns')

        horizscroll_info = Scrollbar(self.master, orient="horizontal")
        horizscroll_info.config(command=self.textbox_info.xview)
        self.textbox_info.config(xscrollcommand=horizscroll_info.set)
        horizscroll_info.grid(column=26, row=2, sticky='nsew', columnspan=12)

        # scroll bar in results text box
        vertscroll_results = Scrollbar(self.master)
        vertscroll_results.config(command=self.textbox_results.yview)
        self.textbox_results.config(yscrollcommand=vertscroll_results.set)
        vertscroll_results.grid(column=39, row=3, sticky='ns')

        horizscroll_results = Scrollbar(self.master, orient="horizontal")
        horizscroll_results.config(command=self.textbox_results.xview)
        self.textbox_results.config(xscrollcommand=horizscroll_results.set)
        horizscroll_results.grid(column=0, row=4, sticky='nsew', columnspan=38)

    def run_program(self):

        # get assembly code input from editor
        code = self.textbox_edit.get('1.0', 'end-1c').split("\n")

        # collect all lines in code for branching
        lines_branching = {}
        for ind, line in enumerate(code):
            lines_branching.update({ind: line})

        # process each line from editor
        for ind, line in enumerate(code):

            ind += 1
            fields = re.split(' |, | ,|,', line)
            fields_ = [field.strip() for field in fields]

            # processing instruction
            self.process_line(line, lines_branching)

            if self.registers["ctrl"] > 0:
                break
            elif self.registers["ctrl"] == 0:

                regs_wout_ctrl = copy.deepcopy(self.registers)
                del regs_wout_ctrl['ctrl']

                # printing results
                self.textbox_results.insert(END, 'line {}: {}\n{}\nOUT:: {}\n\n'.format(ind, fields_, regs_wout_ctrl, self._result))
                self.textbox_info.insert(END, 'line {}:\n{}\n\n{}\n\n'.format(ind, self.memory_addresses.to_string(),self.stack.to_string()))

        # check register for branching
        if self.registers["ctrl"] > 0:
            for ind, line in enumerate(code[self.registers["ctrl"]+1:]):

                fields = re.split(' |, | ,|,', line)
                fields_ = [field.strip() for field in fields]

                ind = ind + self.registers["ctrl"] + 2

                self.process_line(line, lines_branching)

                if self.registers["ctrl"] > 0 and "jmp" in line:
                    break
                elif self.registers["ctrl"] > 0 and "jmp" not in line:

                    regs_wout_ctrl = copy.deepcopy(self.registers)
                    del regs_wout_ctrl['ctrl']

                    # printing results
                    self.textbox_results.insert(END, 'line {}: {}\n{}\nOUT:: {}\n\n'.format(ind, fields_, self.registers, self._result))
                    self.textbox_info.insert(END, 'line {}:\n{}\n\n{}\n\n'.format(ind, self.memory_addresses.to_string(), self.stack.to_string()))

        # jmp instruction code
        if self.registers["ctrl"] > 0 and "jmp" in line:

            fields = re.split(' |, | ,|,', line)
            fields_ = [field.strip() for field in fields]

            ind = ind + self.registers["ctrl"] + 2

            self.process_line(line, lines_branching)

            regs_wout_ctrl = copy.deepcopy(self.registers)
            del regs_wout_ctrl['ctrl']

            # printing results
            self.textbox_results.insert(END, 'line {}: {}\n{}\nOUT:: {}\n\n'.format(ind, fields_, self.registers,
                                                                                    self._result))
            self.textbox_info.insert(END, 'line {}:\n{}\n\n{}\n\n'.format(ind, self.memory_addresses.to_string(),
                                                                          self.stack.to_string()))


    def clear_results(self):
        # clears textbox results
        self.textbox_results.delete('1.0', END)
        self.textbox_info.delete('1.0', END)

        self.memory_arrays = self.memory_arrays_empty
        self.memory_references = self.memory_references_empty

        self.stack = self.stack_empty
        self.stack_items = []
        self.last_address = 0

        self.textbox_results.insert(END, 'line 0: {}\nOUT:: {}\n\n'.format(self.registers_empty, 0))
        self.textbox_info.insert(END, 'line 0:\n{}\n\n{}\n\n'.format(self.memory_addresses.to_string(), self.stack.to_string()))

    def print_to_info(self, text):
        return self.textbox_info.insert(END, '{}\n'.format(text))

    def array_address(self, text):
        terms = re.split('\(|\)', text)
        terms = [t for t in terms if (t != '' and t != ' ')]
        return terms

    def parse_line1(self, line, text):
        terms = line.replace(text, "")
        terms = re.split(', | ,|,', terms)
        terms_ = [t.strip() for t in terms]
        terms_ = [t for t in terms_ if t != '']
        return terms_

    def parse_line2(self, line, text):
        terms = line.replace(text, "")
        terms = re.split(' |, | ,|,', terms)
        terms_ = [t.strip() for t in terms]
        terms_ = [t for t in terms_ if t != '']
        return terms_

    def process_line(self, line, lines_branching):
        """Check for instruction and execute operation in line"""

        # standard arithmetic
        if "add_imm" in line:
            terms_ = self.parse_line1(line=line, text="add_imm")

            # increment a pointer
            if type(self.registers[terms_[0]]) != str:
                self.registers[terms_[0]] = self.registers[terms_[1]] + int(terms_[2])
            elif type(self.registers[terms_[0]]) == str and '0x' not in self.registers[terms_[0]]:
                self.registers[terms_[0]] = hex(int(self.registers[terms_[0]], 16) + 4)

        '''
        if "sub_imm" in line:
            terms_ = self.parse_line1(line=line, text="sub_imm")
            self.registers[terms_[0]] = self.registers[terms_[1]] - int(terms_[2])

        if "mul_imm" in line:
            terms_ = self.parse_line1(line=line, text="mul_imm")
            self.registers[terms_[0]] = self.registers[terms_[1]] * int(terms_[2])

        if "div_imm" in line:
            terms_ = self.parse_line1(line=line, text="div_imm")
            self.registers[terms_[0]] = self.registers[terms_[1]] // int(terms_[2])

            # remainder register
            self.registers["rem"] = self.registers[terms_[1]] % int(terms_[2])
        '''

        # increment and decrement
        if "dec_" in line:
            terms_ = self.parse_line1(line=line, text="dec_")
            self.registers[terms_[0]] = self.registers[terms_[0]] - 1

        if "inc_" in line:
            terms_ = self.parse_line1(line=line, text="inc_")
            self.registers[terms_[0]] = self.registers[terms_[0]] + 1

        # register arithmetic
        if "add_reg" in line:
            terms_ = self.parse_line1(line=line, text="add_reg")
            self.registers[terms_[0]] = self.registers[terms_[1]] + self.registers[terms_[2]]

        if "add_regu" in line:
            terms_ = self.parse_line1(line=line, text="add_regu")
            self.registers[terms_[0]] = self.registers[terms_[1]] + self.registers[terms_[2]]

        if "sub_reg" in line:
            terms_ = self.parse_line1(line=line, text="sub_reg")
            self.registers[terms_[0]] = self.registers[terms_[1]] - self.registers[terms_[2]]

        if "sub_regu" in line:
            terms_ = self.parse_line1(line=line, text="sub_regu")
            self.registers[terms_[0]] = self.registers[terms_[1]] - self.registers[terms_[2]]

        if "mul_reg" in line:
            terms_ = self.parse_line1(line=line, text="mul_reg")
            self.registers[terms_[0]] = self.registers[terms_[1]] * self.registers[terms_[2]]

        if "mul_regu" in line:
            terms_ = self.parse_line1(line=line, text="mul_regu")
            self.registers[terms_[0]] = self.registers[terms_[1]] * self.registers[terms_[2]]

        if "div_reg" in line:
            terms_ = self.parse_line1(line=line, text="div_reg")
            self.registers[terms_[0]] = self.registers[terms_[1]] // self.registers[terms_[2]]

            # remainder register
            self.registers["rem"] = self.registers[terms_[1]] % self.registers[terms_[2]]

        if "div_regu" in line:
            terms_ = self.parse_line1(line=line, text="div_regu")
            self.registers[terms_[0]] = self.registers[terms_[1]] // self.registers[terms_[2]]

            # remainder register
            self.registers["rem"] = self.registers[terms_[1]] % self.registers[terms_[2]]

        # standard arithmetic (unsigned)
        if "add_immu" in line:
            terms_ = self.parse_line1(line=line, text="add_immu")

            # increment a pointer
            if type(self.registers[terms_[0]]) != str:
                self.registers[terms_[0]] = self.registers[terms_[1]] + int(terms_[2])
            elif type(self.registers[terms_[0]]) == str and '0x' not in self.registers[terms_[0]]:
                self.registers[terms_[0]] = hex(int(self.registers[terms_[0]], 16) + 4)

        '''
        if "sub_immu" in line:
            terms = line.replace("sub_immu", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            self.registers[terms_[0]] = self.registers[terms_[1]] - int(terms_[2])

        if "mul_immu" in line:
            terms = line.replace("mul_immu", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            self.registers[terms_[0]] = self.registers[terms_[1]] * int(terms_[2])

        if "div_immu" in line:
            terms = line.replace("div_immu", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            self.registers[terms_[0]] = self.registers[terms_[1]] // int(terms_[2])

            # remainder register
            self.registers["rem"] = self.registers[terms_[1]] % int(terms_[2])
        '''

    #################################################################################

        # standard floating-point arithmetic
        if "fpadd_imm" in line:
            terms_ = self.parse_line1(line=line, text="fpadd_imm")
            self.registers[terms_[0]] = self.registers[terms_[1]] + float(terms_[2])

        if "fpadd_immu" in line:
            terms_ = self.parse_line1(line=line, text="fpadd_immu")
            self.registers[terms_[0]] = self.registers[terms_[1]] + float(terms_[2])

        '''
        if "fpsub_imm" in line:
            terms = line.replace("fpsub_imm", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            self.registers[terms_[0]] = self.registers[terms_[1]] - int(terms_[2])

        if "fpmul_imm" in line:
            terms = line.replace("fpmul_imm", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            self.registers[terms_[0]] = self.registers[terms_[1]] * int(terms_[2])

        if "fpdiv_imm" in line:
            terms = line.replace("fpdiv_imm", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            self.registers[terms_[0]] = self.registers[terms_[1]] // int(terms_[2])

            # remainder register
            self.registers["rem"] = self.registers[terms_[1]] % int(terms_[2])
        '''

        # increment and decrement - floating point
        if "fpdec_" in line:
            terms_ = self.parse_line1(line=line, text="fpdec_")
            self.registers[terms_[0]] = self.registers[terms_[0]] - 1.0

        if "fpinc_" in line:
            terms_ = self.parse_line1(line=line, text="fpinc_")
            self.registers[terms_[0]] = self.registers[terms_[0]] + 1.0

        # register arithmetic
        if "fpadd_reg" in line:
            terms_ = self.parse_line1(line=line, text="fpadd_reg")
            self.registers[terms_[0]] = self.registers[terms_[1]] + self.registers[terms_[2]]

        if "fpadd_regu" in line:
            terms_ = self.parse_line1(line=line, text="fpadd_regu")
            self.registers[terms_[0]] = self.registers[terms_[1]] + self.registers[terms_[2]]

        if "fpsub_reg" in line:
            terms_ = self.parse_line1(line=line, text="fpadd_regu")
            self.registers[terms_[0]] = self.registers[terms_[1]] - self.registers[terms_[2]]

        if "fpsub_regu" in line:
            terms_ = self.parse_line1(line=line, text="fpsub_regu")
            self.registers[terms_[0]] = self.registers[terms_[1]] - self.registers[terms_[2]]

        if "fpmul_reg" in line:
            terms_ = self.parse_line1(line=line, text="fpmul_reg")
            self.registers[terms_[0]] = self.registers[terms_[1]] * self.registers[terms_[2]]

        if "fpmul_regu" in line:
            terms_ = self.parse_line1(line=line, text="fpmul_regu")
            self.registers[terms_[0]] = self.registers[terms_[1]] * self.registers[terms_[2]]

        if "fpdiv_reg" in line:
            terms_ = self.parse_line1(line=line, text="fpdiv_reg")
            self.registers[terms_[0]] = self.registers[terms_[1]] // self.registers[terms_[2]]

        if "fpdiv_regu" in line:
            terms_ = self.parse_line1(line=line, text="fpdiv_regu")
            self.registers[terms_[0]] = self.registers[terms_[1]] // self.registers[terms_[2]]

            # remainder register
            self.registers["rem"] = self.registers[terms_[0]] % self.registers[terms_[1]]

        #################################################################################

        if "cpy" in line:
            terms_ = self.parse_line1(line=line, text="cpy")
            self.registers[terms_[0]] = self.registers[terms_[1]]

        if "print_call" in line:
            self._result = self.registers["out"]

        if "ld_add" in line:
            terms_ = self.parse_line1(line=line, text="ld_add")
            self.registers[terms_[0]] = self.memory_references[terms_[1]][0]

        if "st_wrd" in line:
            terms = line.replace("st_wrd", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]

            parsed = self.array_address(terms_[1])
            item = int(parsed[0]) // 8

            if type(self.registers[terms_[0]]) == int:
                for k, v in self.memory_references.items():
                    if self.registers[parsed[1]] in v:

                        # update memory
                        self.memory_addresses.loc[v[item], '0x4'] = self.registers[terms_[0]]

                        # update array
                        self.memory_arrays[k][item] = self.registers[terms_[0]]

            elif type(self.registers[terms[0]]) == str:
                for k, v in self.memory_references.items():
                    if self.registers[parsed[1]] in v:

                        s = self.registers[terms_[0]].encode('utf-8')

                        # update memory
                        self.memory_addresses.loc[v[item], '0x4'] = s[::-1].hex() + '00'

                        # update array
                        self.memory_arrays[k][item] = s[::-1].hex() + '00'

            # self.textbox_results.insert(END, '{}\n'.format(str(self.memory_references)))
            # self.textbox_results.insert(END, '{}\n'.format(str(self.memory_arrays)))

        if "ld_wrd" in line:
            terms = line.replace("ld_wrd", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]

            parsed = self.array_address(terms_[1])
            item = int(parsed[0]) // 8

            for k, v in self.memory_references.items():
                if self.registers[parsed[1]] in v:
                    if str(self.memory_arrays[k][item]).isdigit():
                        self.registers[terms_[0]] = int(self.memory_arrays[k][item])
                    elif type(self.memory_arrays[k][item]) == str:
                        self.registers[terms_[0]] = self.memory_arrays[k][item]

        if "ld_byte" in line:
            terms_ = self.parse_line1(line=line, text="ld_byte")

        if "no_op" in line:
            pass

        if "brnch" in line:
            terms = line.replace("brnch", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v:
                    self.registers["ctrl"] = k

        if "brnch_e" in line:
            terms = line.replace("brnch_e", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v and (self.registers[terms_[1]] == self.registers[terms_[2]]):
                    self.registers["ctrl"] = k

        if "brnch_ge" in line:
            terms = line.replace("brnch_ge", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v and (self.registers[terms_[1]] >= self.registers[terms_[2]]):
                    self.registers["ctrl"] = k

        if "brnch_le" in line:
            terms = line.replace("brnch_le", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v and (self.registers[terms_[1]] <= self.registers[terms_[2]]):
                    self.registers["ctrl"] = k

        if "brnch_g" in line:
            terms = line.replace("brnch_g", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v and (self.registers[terms_[1]] > self.registers[terms_[2]]):
                    self.registers["ctrl"] = k

        if "brnch_l" in line:
            terms = line.replace("brnch_l", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v and (self.registers[terms_[1]] < self.registers[terms_[2]]):
                    self.registers["ctrl"] = k

        if "brnch_ne" in line:
            terms = line.replace("brnch_ne", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v and (self.registers[terms_[1]] != self.registers[terms_[2]]):
                    self.registers["ctrl"] = k

        if "brnch_nge" in line:
            terms = line.replace("brnch_nge", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v and not (self.registers[terms_[1]] >= self.registers[terms_[2]]):
                    self.registers["ctrl"] = k

        if "brnch_nle" in line:
            terms = line.replace("brnch_nle", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v and not (self.registers[terms_[1]] <= self.registers[terms_[2]]):
                    self.registers["ctrl"] = k

        if "brnch_ng" in line:
            terms = line.replace("brnch_ng", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v and not (self.registers[terms_[1]] > self.registers[terms_[2]]):
                    self.registers["ctrl"] = k

        if "brnch_nl" in line:
            terms = line.replace("brnch_l", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v and not (self.registers[terms_[1]] < self.registers[terms_[2]]):
                    self.registers["ctrl"] = k

        if "brnch_z" in line:
            terms = line.replace("brnch_l", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v and not (self.registers[terms_[1]] == 0):
                    self.registers["ctrl"] = k

        if "brnch_nz" in line:
            terms = line.replace("brnch_l", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v and not (self.registers[terms_[1]] != 0):
                    self.registers["ctrl"] = k

        # string compare
        # pop
        # string add

        if ".arr_" in line:
            terms = line.replace(".arr_", "")
            terms = re.split(' |, | ,|,', terms)
            terms_ = [t.strip() for t in terms]

            length = len(terms_)-1
            self.memory_arrays.update({terms_[0]: terms_[1:]})
            self.memory_references.update({terms_[0]: list(self.memory_addresses.index)[self.last_address:self.last_address+length]})

            for row, item in enumerate(terms_[1:]):
                self.memory_addresses.iloc[self.last_address + row, 0] = item

            # self.textbox_results.insert(END, '{}\n'.format(str(self.memory_references)))
            # self.textbox_results.insert(END, '{}\n'.format(str(self.memory_arrays)))

            self.last_address = self.last_address + row + 1

        if ".str_" in line:
            print(line)
            terms = line.replace(".str_", "")
            terms = re.split(' "|" |"', terms)
            terms_ = [t.strip() for t in terms]
            terms_ = [t.strip('"') for t in terms_]
            terms_ = [t for t in terms_ if t != '']
            print(terms_)

            #self.memory_arrays.update({terms_[0]: terms_[1:]})
            #self.memory_references.update({terms_[0]: list(self.memory_addresses.index)[self.last_address:self.last_address + length]})

            conv_str = '00' + str(binascii.hexlify(bytes(terms_[1][::-1], 'utf-8'))).strip('b').strip("'")
            n = 8
            chunk_string = re.findall('..', conv_str)
            bytes_string = [chunk_string[i * n:(i + 1) * n] for i in range((len(chunk_string) + n - 1) // n)]

            # store string in consecutive memory blocks
            length = len(bytes_string)
            self.memory_arrays.update({terms_[0]: ["".join(item) for item in bytes_string]})
            self.memory_references.update(
                {terms_[0]: list(self.memory_addresses.index)[self.last_address:self.last_address + length]})

            for row, item in enumerate(bytes_string):
                self.memory_addresses.iloc[self.last_address + row, 0] = "".join(item)

        if "sizeof_arr" in line:
            terms = line.replace("sizeof_arr", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            self.registers[terms_[0]] = len(self.registers[terms_[1]])

        if "sizeof_str" in line:
            terms = line.replace("sizeof_str", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            self.registers[terms_[0]] = len(self.registers[terms_[1]])

        # push
        if "push" in line:
            terms = line.replace("push", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            self.stack_items.append(terms_[0])
            self.stack["0x4"] = self.stack_items + ["0x0" for i in range(len(self.stack) - len(self.stack_items))]

        if "jmp" in line:
            terms = line.replace("jmp", "")
            terms = re.split(', | ,|,', terms)
            terms_ = [t.strip() for t in terms]
            for k, v in lines_branching.items():
                if terms_[0] in v:
                    self.registers["ctrl"] = k


if __name__ == "__main__":
    root = Tk()
    root.geometry("+550+300")
    root.title("Arch Assembler")
    app = AssemblyGUI(root)
    root.mainloop()
