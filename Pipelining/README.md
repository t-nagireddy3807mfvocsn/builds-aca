# README #

There are ways to handle datahazards during pipelining in Reduced Instruction Set Computers (RISC) architecture. One solution is to stall the pipeline (no action is taken) for one cycle, while another is to utilize a forwarding unit.

* Forwarding Disabled - Stall Cycle

![Forwarding Disabled - Stall Cycle](images/ForwardDisabled.png)

* Forwarding Enabled - Forwarding Unit

![Forwarding Enabled - Forwarding Unit](images/ForwardEnabled.png)

This python program producing the resulting pipeline with either solution, given a basic set of instuctions as shown in the images above.