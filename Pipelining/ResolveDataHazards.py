
from tkinter import *
import pandas as pd
import re, copy


class Instruction:
    """Represents an instruction and its properties needed to generate timing sequences that solve the data hazards."""

    def __init__(self, instruction_, number):
        self.instruction = instruction_  # string value of instruction
        self.stages = ["F", "D", "X", "M", "W"]
        self.operands = []
        self.opcode = ""
        self.registers = []
        self.instruction_number = number
        self.timing_sequence = self.stages if self.instruction_number == 0 else []

        # collecting instruction info
        self.get_operands()
        self.get_opcode()
        self.get_registers()

        # setting read and write reg
        self.read = [self.registers[0]] if self.opcode == "sw" else self.registers[1:]
        self.write = self.registers[1] if self.opcode == "sw" else self.registers[0]

        # NO FORWARDING or WITHOUT FORWARDING

        if self.opcode != "sw":
            self.available_nf = "W"
            self.needed_nf = "D"
            self.available_nf_ind = 4 if self.instruction_number == 0 else 0
            self.needed_nf_ind = 1 if self.instruction_number == 0 else 0
        elif self.opcode == "sw":
            self.available_nf = "X"
            self.needed_nf = "D"
            self.available_nf_ind = 2 if self.instruction_number == 0 else 0
            self.needed_nf_ind = 1 if self.instruction_number == 0 else 0

        # WITH FORWARDING

        # load
        if self.opcode == "lw":
            self.available_f = "M"
            self.needed_f = "X"
            self.available_f_ind = 3 if self.instruction_number == 0 else 0
            self.needed_f_ind = 2 if self.instruction_number == 0 else 0
        # store
        if self.opcode == "sw":
            self.available_f = "X"
            self.needed_f = "X"
            self.available_f_ind = 2 if self.instruction_number == 0 else 0
            self.needed_f_ind = 2 if self.instruction_number == 0 else 0
        # add, sub, etc.
        if self.opcode != "lw" and self.opcode != "sw":
            self.available_f = "X"
            self.needed_f = "X"
            self.available_f_ind = 2 if self.instruction_number == 0 else 0
            self.needed_f_ind = 2 if self.instruction_number == 0 else 0

    def get_operands(self):
        self.operands = re.split(' |, | ,|,', self.instruction.strip())

    def get_opcode(self):
        self.opcode = self.operands[0]

    def get_registers(self):
        self.registers = re.findall('\$([a,s,t,v][0-9])', self.instruction)

    def generate_timing_sequence(self, previous_timing_sequence):
        self.timing_sequence = ['' for i in range(previous_timing_sequence.index("D"))] + self.stages
        self.update_indexes_nf()
        self.update_indexes_f()

    def insert_stalls_nf(self, prev_inst):
        stalls = prev_inst.available_nf_ind - self.needed_nf_ind  # difference between available and needed
        for i in range(stalls):
            self.timing_sequence.insert(self.needed_nf_ind, "S")
        self.update_indexes_nf()

    def insert_stalls_f(self, prev_inst):
        stalls = prev_inst.available_f_ind - self.needed_f_ind  # difference between available and needed

        # add, sub, etc. after load
        if stalls == 0 and prev_inst.opcode == "lw":
            stalls = stalls + 1

        if "S" in prev_inst.timing_sequence:
            stalls = stalls + 1

        # WAW sw
        # if prev_inst.opcode == "lw" and self.opcode == "sw":

        for i in range(stalls):
            self.timing_sequence.insert(self.needed_f_ind, "S")
        self.update_indexes_f()

    def update_indexes_nf(self):
        self.available_nf_ind = self.timing_sequence.index(self.available_nf)
        self.needed_nf_ind = self.timing_sequence.index(self.needed_nf)

    def update_indexes_f(self):
        self.available_f_ind = self.timing_sequence.index(self.available_f)
        self.needed_f_ind = self.timing_sequence.index(self.needed_f)


class AssemblyGUI:

    def __init__(self, master=None):

        # gui objects
        self.master = master
        self.create_widgets()

    def create_widgets(self):

        # tool bar
        global funit
        funit = IntVar()
        funit.set("1")

        self.button_run = Button(self.master, text="Run", width=8, height=1, command=self.run_program)
        self.button_clear = Button(self.master, text="Clear Results", width=8, height=1, command=self.clear_results)
        self.R1 = Radiobutton(self.master, text="Forwarding Enabled", variable=funit, value=1)
        self.R2 = Radiobutton(self.master, text="Forwarding Disabled", variable=funit, value=2)
        self.button_run.grid(column=0, row=0, sticky='nsew')
        self.button_clear.grid(column=34, row=0, sticky='nsew', columnspan=6)
        self.R1.grid(column=3, row=0, sticky='nsew', columnspan=6)
        self.R2.grid(column=10, row=0, sticky='nsew', columnspan=6)

        # text box to write instructions/code and text box to view results, and textbox to show info
        self.textbox_edit = Text(self.master, height=20, width=40, wrap='none')
        self.textbox_info = Text(self.master, height=20, width=100, wrap='none')
        self.textbox_results = Text(self.master, height=30, width=100, wrap='none')
        self.textbox_edit.grid(column=0, row=1, columnspan=12)
        self.textbox_info.grid(column=14, row=1, columnspan=24)
        self.textbox_results.grid(column=0, row=3, columnspan=38, sticky='nsew')

        # scroll bar in editing text box
        vertscroll_edit = Scrollbar(self.master, orient='vertical')
        horizscroll_edit = Scrollbar(self.master, orient="horizontal")
        vertscroll_edit.config(command=self.textbox_edit.yview)
        horizscroll_edit.config(command=self.textbox_edit.xview)
        self.textbox_edit.config(yscrollcommand=vertscroll_edit.set)
        self.textbox_edit.config(xscrollcommand=horizscroll_edit.set)
        vertscroll_edit.grid(column=13, row=1, sticky='ns')
        horizscroll_edit.grid(column=0, row=2, sticky='nsew', columnspan=12)

        # scroll bar in info text box
        vertscroll_info = Scrollbar(self.master)
        vertscroll_info.config(command=self.textbox_info.yview)
        self.textbox_edit.config(yscrollcommand=vertscroll_info.set)
        vertscroll_info.grid(column=39, row=1, sticky='ns')

        horizscroll_info = Scrollbar(self.master, orient="horizontal")
        horizscroll_info.config(command=self.textbox_info.xview)
        self.textbox_info.config(xscrollcommand=horizscroll_info.set)
        horizscroll_info.grid(column=14, row=2, sticky='nsew', columnspan=24)

        # scroll bar in results text box
        vertscroll_results = Scrollbar(self.master)
        vertscroll_results.config(command=self.textbox_results.yview)
        self.textbox_results.config(yscrollcommand=vertscroll_results.set)
        vertscroll_results.grid(column=39, row=3, sticky='ns')

        horizscroll_results = Scrollbar(self.master, orient="horizontal")
        horizscroll_results.config(command=self.textbox_results.xview)
        self.textbox_results.config(xscrollcommand=horizscroll_results.set)
        horizscroll_results.grid(column=0, row=4, sticky='nsew', columnspan=38)

    def run_program(self):

        code = self.textbox_edit.get('1.0', 'end-1c').split("\n")

        instructions = []
        for line in code:
            instructions.append(line.strip())

        inst_props = {}
        line = 0
        for inst in instructions:
            inst_props[line] = Instruction(inst, line)
            line += 1

        inst_props_noforwarding = copy.deepcopy(inst_props)
        inst_props_forwarding = copy.deepcopy(inst_props)

        self.dependencies(inst_props)

        if funit.get() == 1:  # enabled
            self.textbox_info.insert(END, '\n\nForwarding Enabled \n')
            self.textbox_info.insert(END, '\n\n{}'.format(self.with_forwarding(inst_props_forwarding, instructions).to_string()))

        elif funit.get() == 2:  # disabled
            self.textbox_info.insert(END, '\n\nForwarding Disabled \n')
            self.textbox_info.insert(END, '\n\n{}'.format(self.without_forwarding(inst_props_noforwarding, instructions).to_string()))

    def clear_results(self):

        self.textbox_results.delete('1.0', END)
        self.textbox_info.delete('1.0', END)

    def dependencies(self, inst_props):

        self.textbox_results.insert(END, '\n\nDEPENDENCIES \n')

        for line, instruction in inst_props.items():

            if line == 0:

                self.textbox_results.insert(END, '{} {}\n'.format(line, instruction.instruction))

            elif line == 1:

                # current instruction - instruction from FOR loop
                # previous instruction - inst_props[line-1]

                self.textbox_results.insert(END, '{} {}\n'.format(line, instruction.instruction))

                if inst_props[line - 1].write in instruction.read:  # RAW
                    self.textbox_results.insert(END, "  {} RAW data dependency between instruction {} and instruction {} \n".format(
                        inst_props[line - 1].write, instruction.operands, inst_props[line - 1].operands))

                if instruction.write in inst_props[line - 1].read:  # WAR
                    self.textbox_results.insert(END, "  {} WAR data dependency between instruction {} and instruction {} \n".format(instruction.write,
                                                                                                      instruction.operands,
                                                                                                      inst_props[
                                                                                                          line - 1].operands))

                if instruction.write == inst_props[line - 1].write:  # WAW
                    self.textbox_results.insert(END, "  {} WAW data dependency between instruction {} and instruction {} \n".format(instruction.write,
                                                                                                      instruction.operands,
                                                                                                      inst_props[
                                                                                                          line - 1].operands))

                if len(instruction.read) == 1 and (instruction.read[0] in inst_props[line - 1].read):  # RAR
                    self.textbox_results.insert(END,
                        "  {} RAR data dependency between instruction {} and instruction {} \n".format(instruction.read[0],
                                                                                                    instruction.operands,
                                                                                                    inst_props[
                                                                                                        line - 1].operands))

                elif len(instruction.read) > 1 and ((instruction.read[0] in inst_props[line - 1].read) or (
                        instruction.read[1] in inst_props[line - 1].read)):  # RAR
                    self.textbox_results.insert(END,
                        "  Atleast one register from {} has a RAR data dependency between instruction {} and instruction {} \n".format(
                            instruction.read, instruction.operands, inst_props[line - 1].operands))

            elif line >= 2:

                # current instruction - instruction from FOR loop
                # previous instruction - inst_props[line-1]
                # previous-previous instruction - inst_props[line-2]

                self.textbox_results.insert(END, '{} {}\n'.format(line, instruction.instruction))

                if inst_props[line - 1].write in instruction.read:  # RAW
                    self.textbox_results.insert(END, "  {} RAW data dependency between instruction {} and instruction {} \n".format(
                        inst_props[line - 1].write, instruction.operands, inst_props[line - 1].operands))

                if instruction.write in inst_props[line - 1].read:  # WAR
                    self.textbox_results.insert(END, "  {} WAR data dependency between instruction {} and instruction {} \n".format(instruction.write,
                                                                                                      instruction.operands,
                                                                                                      inst_props[
                                                                                                          line - 1].operands))

                if instruction.write == inst_props[line - 1].write:  # WAW
                    self.textbox_results.insert(END, "  {} WAW data dependency between instruction {} and instruction {} \n".format(instruction.write,
                                                                                                      instruction.operands,
                                                                                                      inst_props[
                                                                                                          line - 1].operands))

                if len(instruction.read) == 1 and (instruction.read[0] in inst_props[line - 1].read):  # RAR
                    self.textbox_results.insert(END,
                        "  {} RAR data dependency between instruction {} and instruction {} \n".format(instruction.read[0],
                                                                                                    instruction.operands,
                                                                                                    inst_props[
                                                                                                        line - 1].operands))

                elif len(instruction.read) > 1 and ((instruction.read[0] in inst_props[line - 1].read) or (
                        instruction.read[1] in inst_props[line - 1].read)):  # RAR
                    self.textbox_results.insert(END,
                        "  Atleast one register from {} has a RAR data dependency between instruction {} and instruction {} \n".format(
                            instruction.read, instruction.operands, inst_props[line - 1].operands))

                if inst_props[line - 2].write in instruction.read:  # RAW in the previous-previous instruction
                    self.textbox_results.insert(END, "  {} POTENTIAL RAW data dependency between instruction {} and instruction {} \n".format(
                        inst_props[line - 2].write, instruction.operands, inst_props[line - 2].operands))


    def without_forwarding(self, inst_props_noforwarding, instructions):

        self.textbox_results.insert(END, '\n\nFORWARDING DISABLED \n')

        for line, instruction in inst_props_noforwarding.items():

            if line == 0:

                self.textbox_results.insert(END, '{} {}\n'.format(line, instruction.instruction))

            elif line == 1:

                self.textbox_results.insert(END, '{} {}\n'.format(line, instruction.instruction))

                # current instruction - instruction from FOR loop
                # previous instruction - inst_props_noforwarding[line-1]

                # generate timing sequence
                instruction.generate_timing_sequence(inst_props_noforwarding[line - 1].timing_sequence)

                if inst_props_noforwarding[line - 1].write in instruction.read:  # RAW
                    self.textbox_results.insert(END, "  {} RAW dependency between instruction {} and instruction {} \n".format(
                        inst_props_noforwarding[line - 1].write, instruction.operands,
                        inst_props_noforwarding[line - 1].operands))

                    # modify the timing sequence
                    if inst_props_noforwarding[line - 1].available_nf_ind > instruction.needed_nf_ind:
                        instruction.insert_stalls_nf(inst_props_noforwarding[line - 1])

                if (instruction.write == inst_props_noforwarding[line - 1].write) and inst_props_noforwarding[line - 1].opcode == "lw":  # WAW
                    if instruction.opcode == "sw":
                        self.textbox_results.insert(END, "  {} WAW data dependency between instruction {} and instruction {} \n".format(
                            instruction.write,
                            instruction.operands,
                            inst_props_noforwarding[line - 1].operands))

                        # modify the timing sequence
                        if inst_props_noforwarding[line - 1].available_nf_ind > instruction.needed_nf_ind:
                            instruction.insert_stalls_nf(inst_props_noforwarding[line - 1])

            elif line >= 2:

                self.textbox_results.insert(END, '{} {}\n'.format(line, instruction.instruction))

                # current instruction - instruction from FOR loop
                # previous instruction - inst_props_noforwarding[line-1]
                # previous-previous instruction - inst_props_noforwarding[line-2]

                # generate timing sequence
                instruction.generate_timing_sequence(inst_props_noforwarding[line - 1].timing_sequence)

                if inst_props_noforwarding[line - 1].write in instruction.read:  # RAW
                    self.textbox_results.insert(END, "  {} RAW dependency between instruction {} and instruction {} \n".format(
                        inst_props_noforwarding[line - 1].write, instruction.operands,
                        inst_props_noforwarding[line - 1].operands))

                    # modify the timing sequence
                    if inst_props_noforwarding[line - 1].available_nf_ind > instruction.needed_nf_ind:
                        instruction.insert_stalls_nf(inst_props_noforwarding[line - 1])

                if inst_props_noforwarding[line - 2].write in instruction.read:  # RAW in the previous-previous instruction
                    self.textbox_results.insert(END, "  {} POTENTIAL RAW dependency between instruction {} and instruction {} \n".format(
                        inst_props_noforwarding[line - 2].write, instruction.operands,
                        inst_props_noforwarding[line - 2].operands))

                    # modify the timing sequence
                    if inst_props_noforwarding[line - 2].available_nf_ind > instruction.needed_nf_ind:
                        instruction.insert_stalls_nf(inst_props_noforwarding[line - 2])

                if (instruction.write == inst_props_noforwarding[line - 1].write) and inst_props_noforwarding[line - 1].opcode == "lw":  # WAW
                    if instruction.opcode == "sw":
                        self.textbox_results.insert(END, "  {} WAW data dependency between instruction {} and instruction {} \n".format(
                            instruction.write,
                            instruction.operands,
                            inst_props_noforwarding[line - 1].operands))

                        # modify the timing sequence
                        if inst_props_noforwarding[line - 1].available_nf_ind > instruction.needed_nf_ind:
                            instruction.insert_stalls_nf(inst_props_noforwarding[line - 1])

        sequences = []
        for k, v in inst_props_noforwarding.items():
            sequences.append(v.timing_sequence)

        no_forwarding = pd.DataFrame(sequences).fillna("")
        no_forwarding["instructions"] = instructions
        no_forwarding.set_index('instructions', inplace=True)

        return no_forwarding


    def with_forwarding(self, inst_props_forwarding, instructions):

        self.textbox_results.insert(END, '\n\nFORWARDING ENABLED \n')

        for line, instruction in inst_props_forwarding.items():

            if line == 0:

                self.textbox_results.insert(END, '{} {}\n'.format(line, instruction.instruction))

            elif line == 1:

                self.textbox_results.insert(END, '{} {}\n'.format(line, instruction.instruction))

                # current instruction - instruction from FOR loop
                # previous instruction - inst_props_forwarding[line-1]

                # generate timing sequence
                instruction.generate_timing_sequence(inst_props_forwarding[line - 1].timing_sequence)

                if inst_props_forwarding[line - 1].write in instruction.read:  # RAW
                    self.textbox_results.insert(END,"  {} RAW dependency between instruction {} and instruction {}\n".format(
                        inst_props_forwarding[line - 1].write, instruction.operands,
                        inst_props_forwarding[line - 1].operands))

                    # modify the timing sequence
                    if inst_props_forwarding[line - 1].available_f_ind >= instruction.needed_f_ind:
                        instruction.insert_stalls_f(inst_props_forwarding[line - 1])

                if (instruction.write == inst_props_forwarding[line - 1].write) and inst_props_forwarding[line - 1].opcode == "lw":  # WAW
                    if instruction.opcode == "sw":
                        self.textbox_results.insert(END, "  {} WAW data dependency between instruction {} and instruction {} \n".format(
                            instruction.write,
                            instruction.operands,
                            inst_props_forwarding[line - 1].operands))

                        # modify the timing sequence
                        if inst_props_forwarding[line - 1].available_nf_ind >= instruction.needed_nf_ind:
                            instruction.insert_stalls_f(inst_props_forwarding[line - 1])

            elif line >= 2:

                self.textbox_results.insert(END, '{} {}\n'.format(line, instruction.instruction))

                # current instruction - instruction from FOR loop
                # previous instruction - inst_props_forwarding[line-1]
                # previous-previous instruction - inst_props_forwarding[line-2]

                # generate timing sequence
                instruction.generate_timing_sequence(inst_props_forwarding[line - 1].timing_sequence)

                if inst_props_forwarding[line - 1].write in instruction.read:  # RAW
                    self.textbox_results.insert(END,"  {} RAW dependency between instruction {} and instruction {}\n".format(
                        inst_props_forwarding[line - 1].write, instruction.operands,
                        inst_props_forwarding[line - 1].operands))

                    # modify the timing sequence
                    if inst_props_forwarding[line - 1].available_f_ind >= instruction.needed_f_ind:
                        instruction.insert_stalls_f(inst_props_forwarding[line - 1])

                if inst_props_forwarding[
                    line - 2].write in instruction.read:  # RAW in the previous-previous instruction
                    self.textbox_results.insert(END,"  {} POTENTIAL RAW dependency between instruction {} and instruction {}\n".format(
                        inst_props_forwarding[line - 2].write, instruction.operands,
                        inst_props_forwarding[line - 2].operands))

                    # modify the timing sequence
                    if inst_props_forwarding[line - 2].available_f_ind >= instruction.needed_f_ind:
                        instruction.insert_stalls_f(inst_props_forwarding[line - 2])

                if (instruction.write == inst_props_forwarding[line - 1].write) and inst_props_forwarding[line - 1].opcode == "lw":  # WAW
                    if instruction.opcode == "sw":
                        self.textbox_results.insert(END, "  {} WAW data dependency between instruction {} and instruction {} \n".format(
                            instruction.write,
                            instruction.operands,
                            inst_props_forwarding[line - 1].operands))

                        # modify the timing sequence
                        if inst_props_forwarding[line - 1].available_nf_ind >= instruction.needed_nf_ind:
                            instruction.insert_stalls_f(inst_props_forwarding[line - 1])

                if "S" in inst_props_forwarding[line - 1].timing_sequence:
                    instruction.insert_stalls_f(inst_props_forwarding[line - 1])

        sequences = []
        for k, v in inst_props_forwarding.items():
            sequences.append(v.timing_sequence)

        with_forwarding = pd.DataFrame(sequences).fillna("")
        with_forwarding["instructions"] = instructions
        with_forwarding.set_index('instructions', inplace=True)

        return with_forwarding


if __name__ == "__main__":
    root = Tk()
    root.geometry("+250+200")
    root.title("TanayNagireddy_Final")
    app = AssemblyGUI(root)
    root.mainloop()
